extends Node

var data : = {}
var relationships : = {}
const DEFAULT_HOSTILE_THRESHOLD = -30
const DEFAULT_ALLY_THRESHOLD = 30
var HOSTILE_THRESHOLD : = DEFAULT_HOSTILE_THRESHOLD
var ALLY_THRESHOLD : = DEFAULT_ALLY_THRESHOLD

func create_faction(name:String) -> void:
	if not data.has(name):
		data[name] = {
			"group":"faction_{name}".format({"name":name}),
			"is_passive":false
		}
func has_faction(name:String) -> bool:
	return data.has(name)
	
func set_faction_passive(name:String, is_passive:bool):
	data[name].is_passive = is_passive

func is_faction_hostile(faction_a:String, faction_b:String) -> bool:
	if faction_a == faction_b:
		return false
	# Check if faction_a is hostile to faction_b, the only reason a->b would, and b->a wouldn't is because b is passive
	var key = [faction_a,faction_b]
	key.sort()
	return (not data[faction_a].is_passive) and relationships.get(key,0.0) < HOSTILE_THRESHOLD

func is_faction_ally(faction_a:String, faction_b:String) -> bool:
	if faction_a == faction_b:
		return true
	# Check if faction_a is hostile to faction_b, the only reason a->b would, and b->a wouldn't is because b is passive
	var key = [faction_a,faction_b]
	key.sort()
	return relationships.get(key,0.0) > ALLY_THRESHOLD 
	
func add_factions_relationship(faction_a:String,faction_b:String,amount:float) -> void:
	if not faction_a == faction_b:
		var key = [faction_a,faction_b]
		key.sort()
		relationships[key] = clamp(relationships.get(key,0.0) + amount,-100,100)
	
func get_faction_relationship(faction_a:String, faction_b:String) -> float:
	if faction_a == faction_b:
		return 100.0
	var key = [faction_a,faction_b]
	key.sort()
	return clamp(relationships.get(key,0.0),-100,100)
	
func add_node_to_faction(node:Node, name:String):
	if not is_faction_hostile_to_node(name,node):
		node.add_to_group(data[name].group, true)

func remove_node_from_faction(node:Node, name:String):
	if data.has(name):
		node.remove_from_group(data[name].group)
func get_nodes_in_faction(name:String) -> Array:
	if not data.has(name):
		return []
	return get_tree().get_nodes_in_group(data[name].group)
	
func is_node_in_faction(node:Node,name:String) -> bool:
	if not node or not data.has(name):
		return false
	return node.is_in_group(data[name].group)
func get_node_factions(node:Node) -> PoolStringArray:
	var res = []
	if node:
		for faction in data:
			if node.is_in_group(data[faction].group):
				res.append(faction) 
	return res

func get_node_relationship_with_faction(node:Node, name:String) -> float:
	if not node or not data.has(name):
		return 0.0
	var worst_case = 100.0
	for faction in get_node_factions(node):
		var relationship = get_faction_relationship(name,faction)
		if relationship < worst_case:
			worst_case = relationship
	return worst_case
	
func get_node_faction_relationships(node:Node) -> Dictionary:
	var res = {}
	if node:
		var factions = get_node_factions(node)
		for name in data:
			var worst_case = 100
			for faction in factions:
				var relationship = get_faction_relationship(name,faction)
				if relationship < worst_case:
					worst_case = relationship
			res[name] = worst_case
	return res

func get_node_faction_relationship_with_node(node_a:Node, node_b:Node):
	if node_a == node_b:
		return 100.0
	if not (node_a and node_b):
		return 0.0
	var worst_case = 100.0
	
	for faction in get_node_factions(node_b):
		var relationship = get_node_relationship_with_faction(node_a,faction)
		if relationship < worst_case:
			worst_case = relationship
	return worst_case
	
func is_faction_hostile_to_node(name:String, node:Node) -> bool:
	if node and data.has(name):
		for faction in get_node_factions(node):
			if is_faction_hostile(name, faction):
				return true
	return false
func is_faction_ally_to_node(name:String, node:Node) -> bool:
	var is_ally = false
	if node and data.has(name):
		for faction in get_node_factions(node):
			if is_faction_hostile(name, faction):
				return false
			if is_faction_ally(name, faction):
				is_ally = true
	return is_ally

func is_node_hostile_to_node(node_a:Node, node_b:Node) -> bool:
	if node_a and node_b and not node_a == node_b:
		for faction in get_node_factions(node_a):
			if is_faction_hostile_to_node(faction,node_b):
				return true
	return false
	
func is_node_ally_to_node(node_a:Node, node_b:Node) -> bool:
	var is_ally = false
	if node_a and node_b:
		if node_a == node_b:
			return true
		
		for faction in get_node_factions(node_a):
			if is_faction_hostile_to_node(faction,node_b):
				return false
			if is_faction_ally_to_node(faction,node_b):
				is_ally = true
	return is_ally

func is_node_passive(node:Node) -> bool:
	for faction in get_node_factions(node):
		if data[faction].is_passive:
			return true
	return false
		
func get_faction_relationships(name:String) -> Dictionary:
	var res = {}
	for b in data:
		if b == name:
			continue
		res[b] = get_faction_relationship(name,b)
	return res
func get_hostile_factions(name:String) -> Dictionary:
	var res = {}
	for b in data:
		if b == name:
			continue
		res[b] = is_faction_hostile(name,b)
	return res

func _new(world_save:ConfigFile):
	
	data = {}
	relationships = {}
	HOSTILE_THRESHOLD = DEFAULT_HOSTILE_THRESHOLD
	ALLY_THRESHOLD = DEFAULT_ALLY_THRESHOLD
	
	create_faction("players")
	create_faction("monsters")
	add_factions_relationship("players","monsters",-100)
	
func _save(world_save:ConfigFile):
	
	world_save.set_value("Factions/config","HOSTILE_THRESHOLD",HOSTILE_THRESHOLD)
	world_save.set_value("Factions/config","ALLY_THRESHOLD",ALLY_THRESHOLD)
	for name in data:
		world_save.set_value("Factions/values",name,data[name])
	for key in relationships:
		world_save.set_value("Factions/relationships",PoolStringArray(key).join("+"),relationships[key])
	
func _load(world_save:ConfigFile):
	
	data = {}
	relationships = {}
	HOSTILE_THRESHOLD = world_save.get_value("Factions/config","HOSTILE_THRESHOLD",DEFAULT_HOSTILE_THRESHOLD)
	ALLY_THRESHOLD = world_save.get_value("Factions/config","ALLY_THRESHOLD",DEFAULT_ALLY_THRESHOLD)
	
	for name in world_save.get_section_keys("Factions/values"):
		var f = world_save.get_value("Factions/values",name)
		create_faction(name)
		set_faction_passive(name,f.is_passive)
	for key in world_save.get_section_keys("Factions/relationships"):
		var value = world_save.get_value("Factions/relationships",key)
		var factions = key.split("+")
		add_factions_relationship(factions[0], factions[1], value)
	
func _ready():
	Saver.connect("new_world",self,"_new")
	Saver.connect("save_world",self,"_save")
	Saver.connect("load_world",self,"_load")
