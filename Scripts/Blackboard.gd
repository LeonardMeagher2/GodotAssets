extends Reference
class_name BlackBoard

var _owner:WeakRef
	
func _get_context():
	var owner = _owner.get_ref()
	if owner and owner.has_meta("blackboard_context"):
		return owner.get_meta("blackboard_context")
	return null

func set_owner(owner:Object):
	_owner = weakref(owner.get_ref()) if owner is WeakRef else weakref(owner)
	if owner and owner.has_meta("blackboard_context") == false:
		owner.set_meta("blackboard_context", BlackboardContext.new())

func get_owner() -> Object:
	if _owner:
		return _owner.get_ref()
	return null

func _get(key):
	return _get_context().data.get(key)

func _set(key, value):
	_get_context().data[key] = value
	return true

func default(key, value) -> bool:
	if not _get_context().data.has(key):
		_get_context().data[key] = value
		return true
	return false
	
func defaults(defaults:Dictionary) -> void:
	for key in defaults:
		if not _get_context().data.has(key):
			_get_context().data[key] = defaults[key]

func has(key) -> bool:
	return _get_context().data.has(key)

func has_all(keys:Array) -> bool:
	return _get_context().data.has_all(keys)

func erase(key) -> bool:
	return _get_context().data.erase(key)

func hash() -> int:
	return _get_context().data.hash()

func empty() -> bool:
	return _get_context().data.empty()
	
func clear() -> void:
	_get_context().data.clear()

func keys() -> Array:
	return _get_context().data.keys()

func size() -> int:
	return _get_context().data.size()

func values() -> Array:
	return _get_context().data.values()

class BlackboardContext extends Reference:
	var data:Dictionary = {}

func _init(owner:Object):
	set_owner(owner)