extends Node

class Context extends Resource:
	var parent = null
	var costs = {}
	var edges = {}
	var weights = {}
	
	var bounds:AABB = AABB()

	func has_edge(from:Vector3, to:Vector3):
		return edges.has(from) and edges[from].has(to)

	func remove_edge(from:Vector3, to:Vector3, bi_directional:bool = false) -> void:
		var key = [from,to]
		if edges.has(from):
			edges[from].erase(to)
			if edges[from].size() == 0:
				edges.erase(from)
		
		if costs.has(key):
			costs.erase(key)
			
		if bi_directional:
			remove_edge(to,from,false)

	
	func add_cost(position:Vector3, type:String, amount:float = 1.0) -> void:
		if not costs.has(type):
			costs[type] = []
		if not costs.has(position):
			costs[position] = {}
			
		var tile = costs.get(position)
		tile[type] = tile.get(type, 0.0) + amount
		
		if costs[type].has(position):
			if tile[type] == 0:
				costs[type].erase(position)
		else:
			costs[type].append(position)


	func add_edge_cost(from:Vector3, to:Vector3, type:String, amount:float = 1.0, bi_directional = false) -> void:
		var key = [from,to]
		
		if not edges.has(from):
			edges[from] = []
		edges[from].append(to)
		
		if not costs.has(type):
			costs[type] = []
		if not costs.has(key):
			costs[key] = {}
		var tile = costs.get(key)
		tile[type] = tile.get(type,0.0) + amount
		
		if costs[type].has(to):
			if tile[type] == 0.0:
				costs[type].erase(to)
		else:
			costs[type].append(to)
			
		if bi_directional:
			add_edge_cost(to,from,type,amount,false)


	func get_edge_cost(from:Vector3, to:Vector3) -> float:
		var key = [from,to]
		var from_tile = costs.get(key,{}).duplicate()
		var to_tile = costs.get(to,{})
	
		for type in to_tile:
			from_tile[type] = from_tile.get(type,0.0) + to_tile[type]
		
		var total = 0.0
		if weights.size() == 0:
			for type in from_tile:
				total += from_tile[type]
		else:
			for type in weights:
				if typeof(type) == TYPE_VECTOR2:
					total += int(to == type) * weights[type]
				elif from_tile.get(type,0.0):
					total += from_tile.get(type,0.0) * weights[type]
		return total


	func get_neighbors(position:Vector3) -> Array:
		var N = position + Vector3( 0, 0,-1)
		var S = position + Vector3( 0, 0, 1)
		var E = position + Vector3( 1, 0, 0)
		var W = position + Vector3(-1, 0, 0)
		var U = position + Vector3( 0, 1, 0)
		var D = position + Vector3( 0,-1, 0)
		
		var neighbors = []
		
		if bounds.has_point(N):
			neighbors.append(N)
		if bounds.has_point(S):
			neighbors.append(S)
		if bounds.has_point(E):
			neighbors.append(E)
		if bounds.has_point(W):
			neighbors.append(W)
		if bounds.has_point(U):
			neighbors.append(U)
		if bounds.has_point(D):
			neighbors.append(D)
		
		for to in edges.get(position,[]):
			if not neighbors.has(to):
				neighbors.append(to)
		
		return neighbors


	func duplicate(deep:bool = false):
		var n = get_script().new()
		n.costs = costs.duplicate(deep)
		n.edges = edges.duplicate(deep)
		n.weights = weights.duplicate(deep)
		return n
		
class DijkstraResult:
	export var came_from:Dictionary
	export var costs:Dictionary
	
	func _init(came_from:Dictionary, costs:Dictionary):
		self.came_from = came_from
		self.costs = costs
		
class DijkstraPath:
	export var costs:Array
	export var total_cost:float
	export var path:Array
	
	func _init(costs:Array, total_cost:float, path:Array):
		self.costs = costs
		self.total_cost = total_cost
		self.path = path
		
func build_dijkstra(context:Context, goals:PoolVector3Array, max_cost:float = INF, flip:bool = false) -> DijkstraResult:
	var frontier = PriorityQueue.new()
	var came_from = {}
	var cost_so_far = {}
	
	for position in goals:
		came_from[position] = null
		cost_so_far[position] = 0.0
		frontier.insert(0.0,position)
	
	while not frontier.empty():
		var current:Vector3 = frontier.pop_front()
		
		for next in context.get_neighbors(current):
			var new_cost = cost_so_far[current]
			if flip:
				new_cost += context.get_edge_cost(current,next)
			else:
				new_cost += context.get_edge_cost(next,current)
			
			if not context.has_edge(current, next):
				# not a custom edge, so we add the cost of a normal neighbor
				new_cost += current.distance_to(next)
			
			if not cost_so_far.has(next) or new_cost < cost_so_far.get(next):
				cost_so_far[next] = new_cost
				if new_cost < max_cost:
					frontier.insert(new_cost,next)
					came_from[next] = current
	
	return DijkstraResult.new(came_from, cost_so_far)

func build_path_from_dijkstra(dijkstra:DijkstraResult, position:Vector3, flip:bool = false) -> DijkstraPath:
	var path = []
	var costs = []
	var total_cost = 0
	
	var prev = position
	var current = dijkstra.came_from.get(prev)
	
	while current != null:
		var cost = dijkstra.costs[prev] - dijkstra.costs[current]
		
		if flip:
			costs.push_front(cost)
			path.push_front(prev)
		else:
			costs.append(cost)
			path.append(current)
		
		total_cost += cost
		prev = current
		current = dijkstra.came_from.get(prev)
			
	return DijkstraPath.new(costs, total_cost, path)


func astar(context:Context, from:Vector3, to:PoolVector3Array, max_cost:float = INF, flip:bool = false) -> DijkstraPath:
	var frontier : = PriorityQueue.new()
	var came_from = {}
	var cost_so_far = {}
	
	for position in to:
		came_from[position] = null
		cost_so_far[position] = 0
		frontier.insert(0,position)
	
	while not frontier.empty():
		var current:Vector3 = frontier.pop_front()
		if current == from:
			break
			
		for next in context.get_neighbors(current):
			
			var new_cost = cost_so_far[current]
			if flip:
				new_cost += context.get_edge_cost(current,next)
			else:
				new_cost += context.get_edge_cost(next,current)
			
			if not context.has_edge(current, next):
				# not a custom edge, so we add the cost of a normal neighbor
				new_cost += current.distance_squared_to(next)
			
			if not cost_so_far.has(next) or new_cost < cost_so_far.get(next):
				cost_so_far[next] = new_cost
				
				if new_cost < max_cost:
					var smallest_distance = INF
					for position in to:
						var distance = position.distance_squared_to(next)
						if distance < smallest_distance:
							smallest_distance = distance
					
					frontier.insert(new_cost + smallest_distance, next)
					came_from[next] = current
	
	return build_path_from_dijkstra(DijkstraResult.new(came_from, cost_so_far), from)
