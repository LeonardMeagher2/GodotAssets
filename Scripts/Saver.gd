extends Node

signal save_world(world_config)
signal load_world(world_config)
signal new_world

var current_world = "testing"
var world_save:ConfigFile = ConfigFile.new()

func get_save_path(filepath:String = "") -> String:
	var parts = filepath.split("/",false)
	
	return "user://save_data/{world}/{path}".format({
		"world":current_world,
		"path":parts.join("/")
	})

func new(world_name:String):
	current_world = world_name
	world_save = ConfigFile.new()
	emit_signal("new_world",world_save)

func save():
	var dir = Directory.new()
	if not dir.dir_exists(get_save_path()):
		dir.make_dir_recursive(get_save_path())
	emit_signal("save_world",world_save)
	world_save.save(get_save_path("global.cfg"))

func load():
	if not world_save.load(get_save_path("global.cfg")) == OK:
		return
	emit_signal("load_world",world_save)
	
func exists(path:String = ""):
	var dir := Directory.new()
	var p = get_save_path(path)
	return dir.dir_exists(p) or dir.file_exists(p)
	
func _ready():
	print(OS.get_user_data_dir())
